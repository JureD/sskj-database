import sqlite3


class SSKJParser:
    def __init__(self):
        # Create database connection
        self.conn = sqlite3.connect('database/sskj.db')
        self.c = self.conn.cursor()

    def isci_po_ID(self, id):
        self.c.execute('SELECT * FROM besede WHERE id=?', (id,))
        beseda = self.c.fetchone()
        return beseda[1]

    def isci_po_dolzini(self, dolzina):
        najdeno = []
        self.c.execute('SELECT * FROM besede')
        besede = self.c.fetchall()
        for zadetek in besede:
            if len(zadetek[1]) == (dolzina + 1):
                najdeno.append(zadetek[1])
            else:
                pass
        return najdeno


def isci_crke_na_mestu(seznam, mesto, crka):
    besede = []
    for beseda in seznam:
        if beseda[mesto - 1] == crka:
            besede.append((beseda))
        else:
            pass
    return besede

def iskanje_z_seznamom_crk(seznam_besed, crke):
    seznam_indeksov_nepravilnih_besed = set([])
    i = 0
    rezultat = []
    while i < len(seznam_besed):
        for crka in crke:
            if crka in seznam_besed[i]:
                seznam_indeksov_nepravilnih_besed.add(i)
        i += 1

    for beseda in seznam_besed:
        if seznam_besed.index(beseda) in seznam_indeksov_nepravilnih_besed:
            pass
        else:
            rezultat.append(beseda)

    return rezultat


slovar = SSKJParser()
iskalni_niz = slovar.isci_po_dolzini(4)
#iskalni_niz = isci_crke_na_mestu(iskalni_niz, 1, 'l')
iskalni_niz = isci_crke_na_mestu(iskalni_niz, 4, 'a')
#iskalni_niz = isci_crke_na_mestu(iskalni_niz, 5, 'n')




mozne_crke = 'clideav'

neprave_crke = []
abeceda = 'abcčdefghijklmnoprsštuvzž'
for i in abeceda:
    if i not in mozne_crke:
        neprave_crke.append(i)

mozne_besede = []
for i in iskalni_niz:
    mozne_besede.append(i.rstrip())

neprave_besede = set([])
for i in mozne_besede:
    for j in neprave_crke:
        if j in i:
            neprave_besede.add(i)

for beseda in mozne_besede:
    if beseda in neprave_besede:
        mozne_besede.remove(beseda)


print([item for item in mozne_besede if item not in neprave_besede])
