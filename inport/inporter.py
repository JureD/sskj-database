import sqlite3

# Create database connection
conn = sqlite3.connect('sskj.db')
c = conn.cursor()

# Create datatable
c.execute('''CREATE TABLE IF NOT EXISTS besede (
    id integer PRIMARY KEY,
    beseda text);''')

# Write datatable to database
conn.commit()

# Pasring .txt file
with open('sskj.txt', encoding='utf8') as sskj:
    for line in sskj:
        vnos = str(line).replace('<br>', '')
        print(vnos)
        c.execute('INSERT INTO besede(beseda) VALUES(?)', (vnos,))

# Closing connection to database
conn.commit()
conn.close()
