Pripomoček za reševanje križank.

V podatkovni bazi je seznam slovenskih besed, prebran s spletne strani: *http://bos.zrc-sazu.si/sbsj.html*
Ob iskanju nastavimo parametre kot so: dolžina besede in črke ki jih že poznamo.
Program vrne seznam besed, ki odgovarjajo iskalnemu nizu.